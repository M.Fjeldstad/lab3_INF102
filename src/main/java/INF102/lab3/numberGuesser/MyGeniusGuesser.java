package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {

        int LOWERBOUND = number.getLowerbound();
        int UPPERBOUND = number.getUpperbound();

        while(LOWERBOUND<=UPPERBOUND){
            int middleNumber = (LOWERBOUND+UPPERBOUND)/2;
            int myGuess = number.guess(middleNumber);

            if(myGuess== 0){
                return middleNumber;
            }else if(myGuess == -1){
                LOWERBOUND = middleNumber +1;
            }else{
                UPPERBOUND = middleNumber - 1;
            }
        } 
        return -1;
    }
}
