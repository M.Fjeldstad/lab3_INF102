package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if(numbers.isEmpty()){
            throw new IllegalArgumentException("The list is empty");
        }
        
        return findMax(numbers, 0, 0);
    }

    private int findMax(List<Integer> numbers, int index, int currentHighNum){
       
        if (index == numbers.size()) {
            return currentHighNum;
        }

        int numAtIndex = numbers.get(index);

        if (numAtIndex> currentHighNum){
            return findMax(numbers, index +1, numAtIndex);
        }else return findMax(numbers, index +1, currentHighNum);
    }

}
